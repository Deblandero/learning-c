#include <stdio.h>

// Write a program to print the corresponding Celsius to Fahrenheit table.

void main()
{
    // print Celsius-Fahrenheit table
    float fahr, celsius;
    int lower, upper, step;

    // lower limite of temperature table
    lower = 0; 
    // upper limit
    upper = 300;
    // step size
    step = 20;

    celsius = lower;
    // Print the heading 
    printf("%s\t%s\n", "C° to", "Fahr");
    while (celsius <= upper)
    {
        // formule (Celsius × 9/5) + 32 = x°F
        fahr = (celsius* 9/5) + 32;
        printf("%6.1f\t%3.0f\n", celsius, fahr);
        celsius = celsius + step;
    }
}