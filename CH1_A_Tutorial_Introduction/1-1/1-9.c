#include <stdio.h>

// Write a program to cpoy its input to its output, replacing each string of one or more blanks by a single blank

void main (){
    //find here : https://stackoverflow.com/a/55804179/10854697

    int c, prevchar;
    while ((c = getchar()) != EOF) {
        if (!(c == ' ' && prevchar == ' ')) {
            putchar(c);
            prevchar = c;
        }
    }

}