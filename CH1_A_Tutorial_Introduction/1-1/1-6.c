#include <stdio.h>

// Verify that the expression getchar() != EOF is 0 or 1.

void main() {
    int c;

    while ((c = getchar()) != EOF)
    {
        printf("EOF is false");
    }

    printf("EOF is true");
}