#include <stdio.h>

// Write a program to count blanks, tabs, and newlines.

void main() {
    int c, tab, bl, nl;

    tab, bl, nl = 0;
    while ((c = getchar()) != EOF)
    {
        if (c == '\n') {
            ++nl;
        } else if (c == '\t'){
            ++tab;
        } else if (c == ' '){
            ++bl;
        }
        printf("%d newlines, %d blanks, %d tab\n", nl, bl, tab);
    }
}