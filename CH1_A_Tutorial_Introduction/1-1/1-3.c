#include <stdio.h>

// Modify the temperature conversion program to print a heading above the table.

void main()
{
    // print Fahrenheit-Celsius table fro fahr = 0, 20, ..., 300
    float fahr, celsius;
    int lower, upper, step;

    // lower limite of temperature table
    lower = 0; 
    // upper limit
    upper = 300;
    // step size
    step = 20;

    fahr = lower;
    // Print the heading 
    printf("%s\t%s\n", "Fahr to", "C°");
    while (fahr <= upper)
    {
        celsius = (5.0/9.0) * (fahr-32.0);
        printf("%3.0f\t%6.1f\n", fahr, celsius);
        fahr = fahr + step;
    }
}