#include <stdio.h>

// Write a program to copy its input to its output replacing eachtab by '\t', each beackspace by '\b', and each backslash by '\\'. 
// This makes tabs and backspaces visible in an unambiguous way.

void main () {
    int c; 

    while ((c = getchar()) != EOF)
    {
        if (c == '  ')
        {
            putchar('\t');
        } else if ( c == '\x08') {
            putchar('\b');
        } else if ( c == '\\\\') {
            putchar('\\')
        } else {
            putchar(c);
        }
        
    }
    
}