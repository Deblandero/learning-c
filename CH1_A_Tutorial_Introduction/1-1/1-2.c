#include <stdio.h>

// Experiment to find out what happens when printf's argument tring countains \c, where c is some character not listed above.

int main()
{	
	// return the error : unknown escape sequence: '\c'
	// /c doesn't exist in set of escape sequences.
	printf("Hello World!\c");
}