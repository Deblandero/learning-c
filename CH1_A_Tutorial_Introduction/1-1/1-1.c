#include <stdio.h>

//Run the "hello world" program on your system. 
//Experiment with leaving out parts of the program, to see what error message you get.

int main(void)
{
	// no error
	printf("Hello world\n");
	return 0;
}